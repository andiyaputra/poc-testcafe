import CreateBannerPage from './Page-Model/CreateBannerPage';
import LoginPage from './Page-Model/loginPage';
import CreateBannerwithAddingRulePage from './Page-Model/CreateBannerwithAddingRulePage';

fixture `BackOffice 3B`
    //.page `http://ifcbuservice.id.test1.alipay.net/login.htm`;//Test
    .page `http://ifcbuservice.id.sit.alipay.net/login.htm`;//SIT

test('As a User I can Create Common/ General Banner', async t => {
   
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
    await CreateBannerPage.createBannerWithoutNewRule_General("[FE AUTO][GENERAL]Banner_withoutAdd_NewRule","COMMON_RULE","https://google.com","https://a.m.dana.id/assets/img/payment-banner/saved-card-banner.png","COMMON FE AUTO");
});
test('As a User I can Create Split Bill Baner Banner', async t=>{
 
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');  
    await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][SPLITBILL]Banner_withoutAdd_NewRule","SPLIT_BILL_RULE","https://google.com","http://a.m.dana.id/resource/imgs/cashier/banner-split-bill.png");
});

test('As a User I can Create Biller Reminder Banner', async t=>{
 
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
    await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][BILLER_REMINDER]Banner_withoutAdd_NewRule","BILLER_REMINDER_RULE","https://google.com","https://a.m.dana.id/assets/img/payment-banner/biller-banner.png");
    
});

test('As a User I can Create IPG Banner', async t=>{
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
    await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][IPG]Banner_withoutAdd_NewRule","IPG_DANA_DOWNLOAD_RULE","https://dana.id","https://a.m.dana.id/assets/img/payment-banner/download-banner.png");  
});

test('As a User I can Create Saving Card Banner', async t=>{
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
    await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][SAVING CARD]Banner_withoutAdd_NewRule","SAVING_CARD_RULE","https://google.com","https://a.m.dana.id/assets/img/payment-banner/saved-card-banner.png");
});

test('As a User I can Update General Banner', async t=>{
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
});

test('As a User I can Update Saving Card/IPG/Biller Reminder/Split Bill Banner', async t=>{
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
});







