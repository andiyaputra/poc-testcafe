import{Selector,t} from 'testcafe';

class CreateBannerwithAddingRulePage {
    constructor () {
        this.nameField = Selector ("#nameInput");
        this.dropdownField = Selector (".ant-select-selection--single");
        this.imageUrlField = Selector("#imageInput");
        this.redirectUrlField = Selector("#urlInput");
        this.selectedRule= Selector("//li[@class='ant-select-dropdown-menu-item-active ant-select-dropdown-menu-item']");
        this.buttonLogin = Selector("#login-btn");
        this.addNewBanner = Selector(".ant-btn-with-icon");
        this.submitButton = Selector("div.ant-layout.ant-layout-has-sider div.ant-layout div.ant-layout-content div.main div.container:nth-child(2) div.wrapper-create-banner form.ant-form.ant-form-horizontal.form-search div.ant-card.ant-card-bordered div.ant-card-body div.card-btn-container:nth-child(4) > button.ant-btn.ant-btn-primary.ant-btn-lg:nth-child(1)");
        //this.assertSuccessBanner = Selector("//center[contains(text(),'SUCCESS')]");
        this.closeButton=Selector("div.ant-modal-wrap.vertical-center-modal.vertical-center-alert-modal div.ant-modal.alert-modal div.ant-modal-content:nth-child(1) div.ant-modal-body div.alert-modal-message div.alert-modal-btn-container.text-center > button.ant-btn.ant-btn-primary.ant-btn-lg")
    }

    async createBannerWithNewRule(bannerName,businessRule,redirectUrl,imageUrl,ctaLabel){
    await t.navigateTo("http://appaymentmng.id.test1.alipay.net/mobile/banner/queryCtaBanner.htm ");
    await t.click(this.addNewBanner);
    await t.typeText(this.NameField,bannerName);
    await t.click(this.businessRuleField);
    await t.typeText(this.businessRuleField,businessRule);
    await t.typeText(this.redirectUrlField,redirectUrl);
    await t.typeText(this.imageUrlField,imageUrl);
    await t.click(this.submitButton).wait(10000);
    //await t.expect(this.assertSuccessBanner.value).eql(SUCCESS);
    await t.click(this.closeButton);

    }
    

}

export default new CreateBannerwithAddingRulePage();