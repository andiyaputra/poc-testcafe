import{Selector,t} from 'testcafe';

class LoginCredential {
    constructor () {
        this.nameField = Selector("#login-name-input");
        this.passField = Selector("#pwd-input");
        this.buttonLogin= Selector("#login-btn");

    }

    
    
}

export default new LoginCredential();