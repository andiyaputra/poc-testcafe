import{Selector,t} from 'testcafe';
class UpdateBannerPage {
    constructor () {
        this.editButton = Selector("div.ant-layout.ant-layout-has-sider div.ant-layout div.ant-layout-content div.main div.container:nth-child(4) div.ant-table-wrapper div.ant-spin-nested-loading div.ant-spin-container div.ant-table.ant-table-large.ant-table-scroll-position-right div.ant-table-content div.ant-table-scroll div.ant-table-body table.ant-table-fixed tbody.ant-table-tbody:nth-child(3) tr.ant-table-row.ant-table-row-level-0:nth-child(1) td.column-min-width:nth-child(7) > a:nth-child(1)")
        this.bannerNameField = Selector ("#nameInput");
        this.businessRuleField = Selector (".ant-select-selection--single");
        this.imageUrlField = Selector("#imageInput");
        this.redirectUrlField = Selector("#urlInput");
        this.selectedRule= Selector("//li[@class='ant-select-dropdown-menu-item-active ant-select-dropdown-menu-item']");
        this.buttonLogin = Selector("#login-btn");
        this.addNewBanner = Selector(".ant-btn-with-icon");
        this.submitButton = Selector("div.ant-layout.ant-layout-has-sider div.ant-layout div.ant-layout-content div.main div.container:nth-child(2) div.wrapper-create-banner form.ant-form.ant-form-horizontal.form-search div.ant-card.ant-card-bordered div.ant-card-body div.card-btn-container:nth-child(4) > button.ant-btn.ant-btn-primary.ant-btn-lg:nth-child(1)");
       //this.assertSuccessBanner = Selector("//center[contains(text(),'SUCCESS')]");
        this.closeButton=Selector("div.ant-modal-wrap.vertical-center-modal.vertical-center-alert-modal div.ant-modal.alert-modal div.ant-modal-content:nth-child(1) div.ant-modal-body div.alert-modal-message div.alert-modal-btn-container.text-center > button.ant-btn.ant-btn-primary.ant-btn-lg");
        this.priorityField=Selector("#priorityInput");
        this.merchantInput =
        this.statusInput=Selector(".ant-select-selection__rendered");
        this.statusActive=Selector(".ant-select-dropdown-menu-item")
    }

    async updateBannerWithoutNewRule_randomBanner(bannerName,priority){
    await t.navigateTo("http://appaymentmng.id.test1.alipay.net/mobile/banner/queryCtaBanner.htm ");
    await t.navigateTo("http://appaymentmng.id.test1.alipay.net/mobile/banner/updateCtaBanner.htm?bannerId=920")
    //await t.click(this.bannerNameField);
    //Format Name : [FE AUTO][UPDATE]
    //await t.click(this.bannerNameField).pressKey('command+a delete');
    await t.doubleClick(this.bannerNameField);
    await t.typeText(this.bannerNameField,bannerName);
    await t.typeText(this.priorityField,priority)
    await t.click(this.statusInput);
    await t.click
    await t.click(this.businessRuleField);
    await t.typeText(this.businessRuleField,businessRule);
    await t.typeText(this.redirectUrlField,redirectUrl);
    await t.typeText(this.imageUrlField,imageUrl);
    await t.click(this.submitButton).wait(10000);
    //await t.expect(this.assertSuccessBanner.value).eql(SUCCESS);
    await t.click(this.closeButton);

    }
    async updateBannerPriority_randomBanner(bannerName,priority){
        await t.navigateTo("http://appaymentmng.id.test1.alipay.net/mobile/banner/queryCtaBanner.htm ");
        await t.navigateTo("http://appaymentmng.id.test1.alipay.net/mobile/banner/updateCtaBanner.htm?bannerId=920")
        //await t.click(this.bannerNameField);
        //Format Name : [FE AUTO][UPDATE]
        //await t.click(this.bannerNameField).pressKey('command+a delete');
        await t.doubleClick(this.bannerNameField).wait(5000);
        await t.typeText(this.bannerNameField,bannerName);
        await t.doubleClick(this.priorityField);
        await t.typeText(this.priorityField,priority);
        await t.click(this.submitButton).wait(10000);
        //await t.expect(this.assertSuccessBanner.value).eql(SUCCESS);
        await t.click(this.closeButton);
    }
    

}

export default new UpdateBannerPage();