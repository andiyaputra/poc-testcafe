import{Selector,t} from 'testcafe';

class LoginPage {
    constructor () {
        this.nameField = Selector("#login-name-input");
        this.passField = Selector("#pwd-input");
        this.buttonLogin= Selector("#login-btn");

    }

    async login_Normal(username,password){
        await t.typeText(this.nameField,username).typeText(this.passField,password).click(this.buttonLogin);
        }

    
}

export default new LoginPage();