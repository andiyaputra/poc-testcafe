import{Selector,t} from 'testcafe';
import LoginPage from './Page-Model/loginPage';
import UpdateBannerPage from './Page-Model/UpdateBannerPage';
fixture `BackOffice 3B`
    .page `http://ifcbuservice.id.test1.alipay.net/login.htm`;

test('Update new banner with valid data', async t => {
   
    await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
    //await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO]Banner_withoutAdd_NewRule","SPLIT_BILL_RULE","https://google.com","http://a.m.dana.id/resource/imgs/cashier/banner-split-bill.png");
    //await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][IPG]Banner_withoutAdd_NewRule","IPG_DANA_DOWNLOAD_RULE","https://google.com","http://a.m.dana.id/resource/imgs/cashier/banner-split-bill.png");
    //await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][SPLITBILL]Banner_withoutAdd_NewRule","SPLIT_BILL_RULE","https://google.com","http://a.m.dana.id/resource/imgs/cashier/banner-split-bill.png");
    //await CreateBannerPage.createBannerWithoutNewRule("[FE AUTO][SAVING CARD]Banner_withoutAdd_NewRule","SAVING_CARD_RULE","https://google.com","https://a.m.dana.id/assets/img/payment-banner/saved-card-banner.png");
    //await UpdateBannerPage.updateBannerWithoutNewRule_randomBanner("-[UPDATED FE AUTO]","1");
    await UpdateBannerPage.updateBannerPriority_randomBanner("-[UPDATED PRIORITY FE AUTO]","1")

});

//test('Adding new banner with add new rule data', async t=>{
 //   await LoginPage.login_Normal('wb-andiyapra474570','Andiy419031996');
   // await CreateBannerwithAddingRulePage.createBannerWithNewRule("[FE AUTO]Banner_withAdd_NewRule","BILLER_REMINDER_RULE","https://google.com","https://a.m.dana.id/assets/img/payment-banner/download-banner.png")


//});